<?php
/**
 * Template Name: Terms Template
 */
?>

<?php while (have_posts()) : the_post(); ?>
    <?php get_template_part('templates/content', 'page-terms'); ?>
<?php endwhile; ?>