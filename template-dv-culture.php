<?php
/**
 * Template Name: DV Culture Template
 */
?>

<?php while (have_posts()) : the_post(); ?>
    <?php get_template_part('templates/content', 'page-dv-culture'); ?>
<?php endwhile; ?>