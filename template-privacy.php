<?php
/**
 * Template Name: Privacy Template
 */
?>

<?php while (have_posts()) : the_post(); ?>
    <?php get_template_part('templates/content', 'page-privacy'); ?>
<?php endwhile; ?>