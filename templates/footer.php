<footer class="footer new-footer">
    <div class="wrapper container-fluid">
        <div class="logo"><a href="<?php the_field('footer_logo_link'); ?>"><img src="<?= get_field('footer_logo')
                ['url']; ?>" alt="DV
        trading"></a></div>
        <div class="row">
            <div class="coll">
                <h4><?php the_field('address_label', 'options'); ?></h4>
                <p>
                    <?php the_field('address_line_1', 'options'); ?><br/>
                    <?php the_field('address_line_2', 'options'); ?><br/>
                    <?php the_field('address_line_3', 'options'); ?>
                </p>
                <p class="tell-box"><a href="tell:3122222222">312-222-2222</a></p>
                <p class="mail-box"><a href="mailto:hello@dvtrading.com">hello@dvtrading.com</a></p>
            </div>
            <div class="coll">
                <h4><?php the_field('social_media_label', 'options'); ?></h4>
                <div class="social-links">
                    <?php
                    if(have_rows('social_media_item', 'options')):
                    while(have_rows('social_media_item', 'options')) : the_row(); ?>
                        <a href="<?php the_sub_field('social_media_link', 'options'); ?>" target="_blank"
                           class="<?php the_sub_field('social_media_class', 'options'); ?>"><?php the_sub_field
                           ('social_media_name', 'options'); ?></a>
                    <?php
                    endwhile;
                    endif;
                    ?>
                </div>
            </div>
            <div class="coll right">
                <?php
                if (has_nav_menu('footer_navigation')) :
                    wp_nav_menu(['theme_location' => 'footer_navigation']);
                endif;
                ?>
                <p>
                    <?php the_field('copyright', 'options'); ?><br/>
                    <?php the_field('copyright_line_2', 'options'); ?><br/>
                    <?php the_field('copyright_line_3', 'options'); ?> <a href="<?php the_field('copyright_link', 'options'); ?>"
                                                               target="_blank"><?php the_field('copyright_link_text', 'options');
                        ?></a>
                </p>
            </div>
        </div>
    </div>
</footer>
</div>
</div>
