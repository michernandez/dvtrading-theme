<section class="bottom-section animated-section" style="background-image: url('<?= get_field('bottom-banner-background-img')['url']; ?>');">
    <div class="globe-box"></div>
    <div class="wrap container-fluid">
        <div class="bottom-section-content">
            <p><?php the_field('bottom_banner_text', 'options'); ?></p>
            <a href="<?php the_field('dvtrading.dev/contact', 'options') ?>" class="btn"><?php the_field('get_in_touch_text', 'options');
                ?></a>
        </div>
    </div>
</section>