<div class="main page about-page" id="main">
    <div class="top-banner" style="background-image: url('images/about-bg.jpg');">
        <div class="animated-text-wrapp">
            <div class="animated-text-box">
                <span class="animated-text" id="animated-text4">
                    <?php the_field('about_page_title'); ?>
                </span>
            </div>
        </div>
    </div>
    <section class="description-section animated-section">
        <div class="wrap container-fluid">
            <div class="title-box">
                <?php the_field('our_mission_title') ?>
            </div>
            <div class="content-box">
                <p>
                    <?php the_field('our_mission_text') ?>
                </p>
            </div>
        </div>
    </section>

    </div>
    <div class="history-section">
        <div class="wrap container-fluid">
            <h2>Our <br/>History</h2>
            <div class="history-slider">
                <div class="frame" id="basic">
                    <ul class="clearfix">
                        <?php
                        if(have_rows('our_history_items')) :
                        while(have_rows('our_history_items')) : the_row();
                        ?>
                            <li>
                                <div class="history-image">
                                    <img src="<?= get_sub_field('history_item_image')['url'] ?>" alt="Image">
                                </div>
                                <div class="history-content">
                                    <span class="title"><?php the_sub_field('history_item_year') ?></span>
                                    <span class="content-slider">
								<?php the_sub_field('history_item_text') ?>
							</span>
                                </div>
                            </li>
                        <?php
                        endwhile;
                        endif;
                        ?>
                    </ul>
                </div>
                <div class="scrollbar">
                    <div class="handle">
                        <div class="mousearea"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php
    get_template_part('templates/modules/bottom', 'banner');
    ?>
</div>
<?php get_template_part('footer.php') ?>