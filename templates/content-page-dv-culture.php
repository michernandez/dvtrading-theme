<div class="main page culture-page" id="main">
    <div class="top-banner" style="background-image: url('images/dv-culture-top.jpg');">
        <div class="animated-text-wrapp">
            <div class="animated-text-box">
                <span class="animated-text" id="animated-text2">
                    <?php the_field('dv_culture_title'); ?>
                </span>
            </div>
        </div>
    </div>
    <section class="description-section">
        <div class="wrap container-fluid">
            <div class="title-box">
                Work/Life Balance
            </div>
            <div class="content-box">
                <p>
                    Mauris id&nbsp;augue sed purus gravida elementum. Quisque scelerisque in&nbsp;mauris non tempor. Duis vulputate, mauris in&nbsp;convallis sagittis, nisi lorem commodo quam, et&nbsp;interdum massa elit at&nbsp;ex. Nulla dignissim, risus quis maximus imperdiet, erat risus semper velit, ut&nbsp;convallis mi&nbsp;mi&nbsp;ut&nbsp;nisl.
                </p>
            </div>
        </div>
    </section>
    <div class="image-section" style="background-image: url('images/dv-culture-1.jpg');">
    </div>
    <section class="our-team-section animated-section">
        <div class="wrap container-fluid">
            <div class="our-team-wrapp">
                <div class="team-content">
                    <div class="team-item-wrapp">
                        <div class="team-item">
                            <div class="team-item-quote">
                                &ldquo;Mauris id&nbsp;augue sed purus gravida elementum. Quisque scelerisque in&nbsp;mauris non tempor. Duis vulputate, mauris in&nbsp;convallis sagittis, nisi lorem commodo quam, et&nbsp;interdum massa elit at&nbsp;ex.
                            </div>
                            <div class="team-item-author">
                                <div class="author-name">
                                    <strong>LINDA SMITH</strong> - TITLE
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="triangle-box">
                        <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                             viewBox="0 0 96 87" enable-background="new 0 0 96 87" xml:space="preserve">
						<title>Triangle</title>
                            <desc>Created with Avocode.</desc>
                            <path id="SvgjsPath1020" fill="#FFFFFF" fill-opacity="0" stroke="#262E97" stroke-width="6" stroke-miterlimit="50" stroke-dasharray="0" d="M48,79.4L7.8,3.6h80.4L48,79.4z" style="stroke-dasharray: 252.00048828125px, 252.00048828125px; stroke-dashoffset: 5px;stroke-width:6;stroke-linecap:round;stroke-linejoin:miter;stroke-miterlimit:4;stroke-dasharray:none"/>
						</svg>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <div class="image-gallery-section animated-section">
        <div class="wrap container-fluid">
            <div class="image-gallery-section-wrapp">
                <div class="gallery-item">
                    <img src="images/dv-culture-2.jpg" alt="Image"/>
                </div>
                <div class="gallery-item">
                    <img src="images/dv-culture-3.jpg" alt="Image"/>
                </div>
                <div class="gallery-item">
                    <img src="images/dv-culture-4.jpg" alt="Image"/>
                </div>
                <div class="gallery-item">
                    <img src="images/dv-culture-5.jpg" alt="Image"/>
                </div>
                <div class="gallery-item">
                    <img src="images/dv-culture-6.jpg" alt="Image"/>
                </div>
                <div class="gallery-item">
                    <div class="gallery-item-content">
                        <div class="gallery-item-quote">
                            &ldquo;Mauris id&nbsp;augue sed purus gravida elementum. Quisque scelerisque in&nbsp;mauris non tempor. Duis vulputate, mauris in&nbsp;convallis sagittis, nisi lorem commodo quam, et&nbsp;interdum massa elit at&nbsp;ex. Nulla dignissim, risus quis maximus imperdiet, erat risus.&rdquo;
                        </div>
                        <div class="gallery-item-author">
                            <div class="author-name">
                                <strong>LINDA SMITH</strong> - TITLE
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php
    get_template_part('templates/modules/bottom', 'banner');
    ?>
</div>