<div class="main page energy-page" id="main">
    <div class="top-banner" style="background-image: url('images/dv-energy-bg.jpg');">
        <div class="animated-text-wrapp">
            <div class="animated-text-box">
                <span class="animated-text" id="animated-text3"></span>
            </div>
        </div>
    </div>
    <section class="description-section animated-section">
        <div class="wrap container-fluid">
            <div class="title-box">
                What We Trade
            </div>
            <div class="content-box">
                <p>
                    DV&nbsp;Energy is&nbsp;a&nbsp;division of&nbsp;DV&nbsp;Trading that specializes in&nbsp;the trading of&nbsp;financial and physical crude oil, refined products and natural gas. As&nbsp;a&nbsp;liquidity provider, DV&nbsp;Energy is&nbsp;a&nbsp;reliable counter party who utilizes a&nbsp;comprehensive analytical approach and top tier technology to&nbsp;accept the transfer of&nbsp;risk in&nbsp;all market conditions.
                </p>
                <div class="triangle-box">
                    <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                         viewBox="0 0 96 87" enable-background="new 0 0 96 87" xml:space="preserve">
					<title>Triangle</title>
                        <desc>Created with Avocode.</desc>
                        <path id="SvgjsPath1020" fill="#FFFFFF" fill-opacity="0" stroke="#262E97" stroke-width="6" stroke-miterlimit="50" stroke-dasharray="0" d="M48,79.4L7.8,3.6h80.4L48,79.4z" style="stroke-dasharray: 252.00048828125px, 252.00048828125px; stroke-dashoffset: 5px;stroke-width:6;stroke-linecap:round;stroke-linejoin:miter;stroke-miterlimit:4;stroke-dasharray:none"/>
					</svg>
                </div>
            </div>
        </div>
    </section>
    <div class="image-section" style="background-image: url('images/dv-energy-bg.jpg');">
    </div>
    <div class="image-gallery-section animated-section">
        <div class="wrap container-fluid">
            <div class="image-gallery-box">
                <div class="image-gallery-section-wrapp">
                    <div class="gallery-item">
                        <img src="images/dv-energy-1.jpg" alt="Image"/>
                    </div>
                    <div class="gallery-item">
                        <img src="images/dv-energy-2.jpg" alt="Image"/>
                    </div>
                    <div class="gallery-item">
                        <img src="images/dv-energy-3.jpg" alt="Image"/>
                    </div>
                    <div class="gallery-item">
                        <img src="images/dv-energy-4.jpg" alt="Image"/>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php
    get_template_part('templates/modules/bottom', 'banner');
    ?>
</div>