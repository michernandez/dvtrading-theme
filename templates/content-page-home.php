<div class="main" id="main">
    <div class="top-banner" style="background-image: url('<?= get_field('top_background')['url']; ?>');">
        <div class="top-info-content animated-section">
            <div class="top-info-wrapp">
                <p><?php the_field('bottom_plain_text'); ?> <br/><a href="<?php the_field('bottom_link'); ?>"><?php
                        the_field('bottom_link_text');
                        ?></a></p>
            </div>
        </div>
        <div class="animated-text-wrapp">
            <div class="animated-text-box">
                <span class="animated-text" id="animated-text1">
                    <?php
                    if(have_rows('home_animated_text')) :
                    while(have_rows('home_animated_text')) : the_row();
                    ?>
                    <span><?php the_sub_field('animated_word'); ?></span>
                    <?php
                    endwhile;
                    endif;
                    ?>
                </span>
            </div>
        </div>
        <div class="info-wrapp">
            <div id="hero1">
                <div class="swiper-container">
                    <div class="swiper-wrapper">
                        <?php
                        if(have_rows('slider_item')):
                            while(have_rows('slider_item')) : the_row();?>
                                <div class="swiper-slide">
                                    <div class="info-content">
                                        <a href="<?php the_sub_field('item_link'); ?>" target="_blank">
								<span class="h3">
									<?php the_sub_field('item_title'); ?>
								</span>
                                            <span class="date-box">
									<?php the_sub_field('item_date'); ?>
								</span>
                                            <span class="slider-cell">
									<span class="p">
										<?php the_sub_field('item_text') ?>
									</span>
									<span class="more">READ MORE</span>
								</span>
                                        </a>
                                    </div>
                                </div>
                        <?php
                            endwhile;
                        endif;
                        ?>
                    </div>
                </div>
                <div class="pagination"></div>
                <div class="swiper-button-next"></div>
                <div class="swiper-button-prev"></div>
            </div>
        </div>
    </div>
    <section class="description-section animated-section">
        <div class="wrap container-fluid">
            <div class="title-box">
                <?php the_field("what_we_do_title"); ?>
            </div>
            <div class="content-box">
                <p>
                    <?php the_field('what_we_do_text'); ?>
                </p>
                <a href="<?php the_field('learn_more_link'); ?>" class="more"><?php the_field('learn_more_text');
                ?></a>
            </div>
        </div>
    </section>
    <div class="image-section" style="background-image: url('images/temp-1.jpg');">
    </div>
    <section class="our-team-section animated-section">
        <div class="wrap container-fluid">
            <div class="our-team-wrapp">
                <div class="team-content">
                    <h4><?php the_field('join_title') ?></h4>
                    <div class="team-item-wrapp">
                        <?php
                        if(have_rows('quotes')):
                            while(have_rows('quotes')) : the_row();?>
                                <div class="team-item">
                                    <div class="team-item-quote">
                                        <?php the_sub_field('quote_text'); ?>
                                    </div>
                                    <div class="team-item-author">
                                        <img src="<?= get_sub_field('quote_author_img')['url']; ?>" alt="Image"
                                             class="author-img"/>
                                        <div class="author-name">
                                            <strong><?php the_sub_field('quote_author'); ?></strong> - <?php
                                            the_sub_field('quote_author_title'); ?>
                                        </div>
                                    </div>
                                </div>
                        <?php
                            endwhile;
                        endif;
                        ?>
                        <a href="<?php the_field('join_link'); ?>" class="btn"><?php the_field('join_button_text');
                            ?></a>
                    </div>
                </div>
                <div class="team-image">
                    <img alt="Image" src="<?= get_field('join_adjacent_img')['url']; ?>">
                </div>
                <div class="team-bottom-box">
                    <div class="team-item">
                        <div class="team-item-quote">
                            &ldquo;Mauris id&nbsp;augue sed purus gravida elementum. Quisque scelerisque in&nbsp;mauris non tempor.&rdquo;
                        </div>
                        <div class="tw-item-author">
                            <div class="tw-name">@DVTRADING </div><div class="tw-time">&bull; 1&nbsp;MINUTE AGO</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="new-section animated-section">
        <div class="wrap container-fluid">
            <div class="title-section">
                <h4><?php the_field('home_latest_news_title'); ?></h4>
                <a class="more">SEE MORE NEWS</a>
            </div>
            <div class="new-section-wrapp">
                <div class="info-box ">
                    <?php
                    $news = get_field("news_post");
                    if($news):
                    foreach($news as $post): ?>
                        <?php setup_postdata($post) ?>
                    <div class="info-box-item ">
                        <a class="info-box-item-wrap" href="<?php the_field('news_link'); ?>" target="_blank">
                        <span class="date-box">
                            <?php the_field('news_date'); ?>
                        </span>
                            <span class="h3">
                            <?= get_the_title(); ?>
                        </span>
                            <span class="p">
                            <?php the_field('news_text'); ?>
                        </span>
                        </a>
                    </div>
                    <?php endforeach;
                    wp_reset_postdata();
                    endif;
                    ?>
                </div>
            </div>
        </div>
    </section>
    <?php
    get_template_part('templates/modules/bottom', 'banner');
    ?>
</div>
<?php get_template_part('footer.php') ?>