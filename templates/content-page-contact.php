<div class="main main-contact new-contact">
    <div class="img-box">
        <img src="./images/contact-1.jpg" class="item-1"/>
        <img src="./images/contact-2.jpg" class="item-2"/>
        <img src="./images/contact-3.jpg" class="item-3"/>
    </div>
    <div class="wrap container-fluid clearfix">
        <h2>CONTACT</h2>
        <div class="contact-content">
            <div class="contact-info">
                <ul>
                    <li class="current">
                        <a href=".item-1">
                            <img src="images/chicago.jpg" alt="CHICAGO">
                            <span class="title-info">
                                CHICAGO
                            </span>
                            <span class="contact-info-box">
                                216 West Jackson Boulevard<br/>
                                3rd Floor<br/>
                                Chicago, Illinois, 60606
                            </span>
                        </a>
                    </li>
                    <li>
                        <a href=".item-2">
                            <img src="images/new-york.jpg" alt="NEW YORK">
                            <span class="title-info">
                                NEW YORK
                            </span>
                            <span class="contact-info-box">
                                5 Hanover Square<br/>
                                Suite 2101<br/>
                                New York, NY, 10004
                            </span>
                        </a>
                    </li>
                    <li>
                        <a href=".item-3">
                            <img src="images/toronto.jpg" alt="TORONTO">
                            <span class="title-info">
                                TORONTO
                            </span>
                            <span class="contact-info-box">
                                370 King St West Suite 701<br/>
                                Toronto, Ontario<br/>
                                M5J 1V9
                            </span>
                        </a>
                    </li>
                </ul>
            </div>
            <div class="contact-form">
                <form id="contact-form" action="javascript:;">
                    <span class="cell-form">
                        <p>FIELD REQUIRED</p>
                        <label for="name">NAME</label>
                        <input type="text" name="name" id="name" value="" required>
                    </span>
                    <span class="cell-form">
                        <p>FIELD REQUIRED</p>
                        <label for="email">EMAIL</label>
                        <input type="email" name="email" id="email" value="" required>
                    </span>
                    <span class="cell-form">
                        <p>FIELD REQUIRED</p>
                        <label for="in">INTERESTED IN</label>
                        <select id="in" name="interested-in" required>
                            <option value="">Select One</option>
                            <option>New Business</option>
                            <option>Join Our Team</option>
                            <option>Internships</option>
                            <option>Press Inquiries</option>
                            <option>Other</option>
                        </select>
                    </span>
                    <span class="cell-form">
                        <label for="question">QUESTION OR COMMENT</label>
                        <textarea id="question" name="question"></textarea>
                    </span>
                    <span class="cell-form checkbox-form">
                        <input type="checkbox" id="ch" name="ch1" checked="checked" value="yes">
                        <label for="ch">Receive DV Trading News</label>
                    </span>
                    <input type="submit" value="SUBMIT"/>
                </form>
                <div class="message"></div>
            </div>
        </div>
    </div>
</div>