<div class="new-wrapper">
    <header class="header new-header">
        <div class="wrapper-box">
            <h1 class="logo"><a href="<?= get_site_url(); ?>"><img src="<?= get_field('header_logo', 'options')
                    ["url"]; ?>" alt="DV
            trading"></a></h1>
            <div class="right">
                <a href="#" class="nav-toggle">menu</a>
                <a href="dvtading.dev/contact" class="btn"><?= get_field('contact_us_text', 'options'); ?></a>
                <div class="main-nav">
                    <a href="#" class="nav-close">Close</a>
                    <?php
                    if (has_nav_menu('primary_navigation')) :
                        wp_nav_menu(['theme_location' => 'primary_navigation']);
                    endif;
                    ?>
                </div>
            </div>
        </div>
    </header>