<?php
/**
 * Template Name: DV Energy Template
 */
?>

<?php while (have_posts()) : the_post(); ?>
    <?php get_template_part('templates/content', 'page-dv-energy'); ?>
<?php endwhile; ?>