(function(factory) {
	if( window.Clique.support.hasTouch && document.querySelector('.hamburger') ) {
		factory(window.Clique);
	}
})(function(_c) {

	_c.$body.swipe({
		swipe:function(event, direction, distance, duration, fingerCount, fingerData) {

			// vars
			var $button = _c.$('.hamburger');

			// init
			if( direction === 'down' && ! $button.hasClass('is-active') ) {
				$button.addClass('is-active');
				_c.$html.addClass('menu-open');
				$('.topnav').slideDown(600);
			} else if( direction === 'up' && $button.hasClass('is-active')  ) {
				$button.removeClass('is-active');
				_c.$html.removeClass('menu-open');
				$('.topnav').slideUp(600);
			}
		},
		threshold : window.innerHeight / 3
	});
});
