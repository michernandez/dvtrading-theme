(function ($) {
	'use strict';

	var TypingAnimation = function(options)
	{
		var $container = $('body'), $cursor = null;
		var generateCharacter = function(character, addonClass) {
			var code = '';

			if(character == '.'){
				code = '<span class="char char-dot' + (typeof(addonClass) != undefined ? ' ' + addonClass : '') + '"></span>';
			} else if(character == ' '){
				code = '<span class="char char-space' + (typeof(addonClass) != undefined ? ' ' + addonClass : '') + '"></span>';
			} else {
				code = '<span class="char' + (typeof(addonClass) != undefined ? ' ' + addonClass : '') + '">' + character + '</span>';
			}

			return code;
		};

		var generatePhrases = function() {
			var code = '<span class="spacer">&nbsp;</span>';
			for(var i = 0; i < options.phrases.length; i++){
				var phrase = options.phrases[i];

				var charactersCode = "";
				for(var j = phrase.length; j >= 0; j--){
					charactersCode += generateCharacter(phrase.charAt(j), ' char-rev');
				}
				code += '<div class="phrase">' + charactersCode + "</div>";

				charactersCode = "";
				for(var j = 0; j < phrase.length; j++){
					charactersCode += generateCharacter(phrase.charAt(j), ' char-normal');
				}
				code += '<div class="phrase-buffer">' + charactersCode + "</div>";
			}

			$container.html(code);
			$container.children('.phrase, .phrase-buffer').hide();
			$container.children('.phrase').hide();

			var cursorCode = '<div class="cursor"></div>';
			$container.append(cursorCode);
			$cursor = $container.find('.cursor');
		};
		var getVisiblePhrase = function()
		{
			return $container.find('.phrase:visible');
		};

		var getFirstPhrase = function()
		{
			return $container.find('.phrase:first');
		};

		var hidePhrase = function($phrase, timeToShowNextPhrase)
		{
			var totalCharacters = $phrase.next(".phrase-buffer").children('.char').length;
			$phrase.next(".phrase-buffer").children('.char').each(function(itemNumber){
				var $character = $(this);

				$character.delay(itemNumber * 200).animate({
					top: "+=30",
					opacity: 0
				}, "fast", function(){
					if(itemNumber == totalCharacters - 1){
						timeToShowNextPhrase();
						if(typeof(options.onNextPhrase) == 'function'){
							options.onNextPhrase();
						}
					}
				});
			});
		};

		var animateCursor = function($visiblePhrase)
		{
			var phraseWidth = parseInt($visiblePhrase.width(), 10);
			$cursor.css({ left: 0 }).fadeIn("fast", function(){
				$cursor.animate({
					left: phraseWidth
				}, { duration: options.nextPhraseDelay, queue: false });

				var totalChildren = $visiblePhrase.next(".phrase-buffer").children('.char').length;
				$visiblePhrase.next(".phrase-buffer").children('.char').each(function(i){
					$(this).css('opacity', 0.3 + i / totalChildren / 1.5);
				});

				$visiblePhrase.next(".phrase-buffer").show().css('left', 0).animate({
					width: phraseWidth
				}, { duration: options.nextPhraseDelay, queue: false });

				$visiblePhrase.animate({
					width: 0,
					left: phraseWidth
				}, { duration: options.nextPhraseDelay, queue: false });

			});
		};

		var rewindCursor = function()
		{
			$cursor.delay(500).hide();
		};

		var firstTime = true;
		var gotoNextPhrase = function() {
			var $currentPhrase = getVisiblePhrase();

			var normalizeHiddenPhrase = function() {
				$currentPhrase.hide();
				$currentPhrase.children('.char').css({
					opacity: 1,
					top: 0,
				});
				$currentPhrase.css({
					left: 0,
					width: "auto"
				});

				$currentPhrase.next('.phrase-buffer').hide();
				$currentPhrase.next('.phrase-buffer').children('.char').css({
					opacity: 1,
					top: 0,
				});
				$currentPhrase.next('.phrase-buffer').css({
					left: 0,
					width: "auto"
				});
			};

			var showNextPhraseAction = function() {
				var $nextPhrase = $currentPhrase.next().next('.phrase');
				if(!$nextPhrase.length){
					$nextPhrase = getFirstPhrase();
				}

				normalizeHiddenPhrase();
				$nextPhrase.fadeIn("fast", function () {
					$nextPhrase.next('.phrase-buffer').show().css("width", 0);
					animateCursor($nextPhrase);
				});

				setTimeout(function(){
					gotoNextPhrase();
				}, options.nextPhraseDelay);
			};

			if(!$currentPhrase.length) {
				$currentPhrase = getFirstPhrase();
				$currentPhrase.show();

				setTimeout(function(){
					gotoNextPhrase();
				}, options.nextPhraseDelay);
			} else {
				rewindCursor();

				if(options.phrases.length == 1) {
					setTimeout(function(){
						hidePhrase($currentPhrase, showNextPhraseAction);
					}, 2500);
				} else {
					hidePhrase($currentPhrase, showNextPhraseAction);
				}
			}

			if(firstTime) {
				firstTime = false;
				animateCursor($currentPhrase);
			}
		};

		var __constructor = function() {
			var defaults = {
				container: '',
				phrases: [],
				nextPhraseDelay: 3500,
				onNextPhrase: null
			};

			options = $.extend({}, defaults, options);

			if(!$(options.container).length){
				console.error("Container for Typing Animation is missing.");
				return;
			}

			$container = $(options.container);

			if(!options.phrases.length){
				console.error("Phrases are empty.");
				return;
			}

			generatePhrases();
			gotoNextPhrase();
		};

		__constructor();
	};

	window.Clique.TypingAnimation = TypingAnimation;
})(window.jQuery);
