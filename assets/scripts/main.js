


/* ========================================================================
 * DOM-based Routing
 * Based on http://goo.gl/EUTi53 by Paul Irish
 *
 * Only fires on body classes that match. If a body class contains a dash,
 * replace the dash with an underscore when adding it to the object below.
 *
 * .noConflict()
 * The routing is enclosed within an anonymous function so that you can
 * always reference jQuery with $, even when in .noConflict() mode.
 * ======================================================================== */




(function ($, _c, undefined) {
	'use strict';

	// Use this variable to set up the common and page specific functions. If you
	// rename this variable, you will also need to rename the namespace below.

	var Sage = {
		// All pages
		'common': {
			init: function() {
				// JavaScript to be fired on all pages

				// disable the page loader
				if( ! _c.support.transition || ! _c.support.transition.end ) {
					_c.showLoading = false;
				}

				// Hamburger Menu
				(function() {
					$('.header').on('click', '.hamburger', function(e){
						e.preventDefault();
						e.stopPropagation();
						if ($(this).hasClass('is-active')) {
							$('.topnav').slideDown(600);
						} else {
							$('.topnav').slideUp(600);
						}
					});
					$(window).resize(function(){
						$('.topnav').attr('style', '');
						$('.hamburger').removeClass('is-active');
					});
				})();


				// Submenu
				function openMenu(){
					$('.submenu').slideDown(300);
				}
				function closeMenu(){
					$('.submenu').slideUp(300);
				}
				function noMenu(){

				}
				function submenu(){
					if ($(window).innerWidth() > 768){
						$(".topnav .parent").hoverIntent(openMenu, closeMenu);
					} else {
						$(".topnav .parent").hoverIntent(noMenu);
					}
				}
				submenu();

				var resizeTimer;

				$(window).on('resize', function(e) {
					clearTimeout(resizeTimer);
					resizeTimer = setTimeout(function() {
						submenu();
						if ($(window).innerWidth() < 768){
							$('.topnav .parent a').on('click', function(){
								if (!$(this).hasClass('open')){
									$(this).addClass('open');
									$(this).next().slideDown(300);
								} else {
									$(this).removeClass('open');
									$(this).next().slideUp(300);
								}
							});
						}
					}, 250);

				});

				// Form handler
				(function(){
					$('form#contact-form').on('submit', function(){
						$('.cell-form').removeClass('error');
						var form = $(this);

						if($('input.error').length || $('select.error').length){
							$('select.error').closest('.cell-form').addClass('error');
							$('input.error').closest('.cell-form').addClass('error');

							return false;
						}

						$('.message').html('Please wait...').removeClass('error').show();
						$('html, body').animate({ scrollTop: $($('.message')).offset().top}, 500);

						$.post('../send-mail.php', form.serialize(), function(data){
							if(data == 'DONE'){
								$('.message').html('Successfully sent').removeClass('error').show();
								$('html, body').animate({ scrollTop: $($('.message')).offset().top}, 500);
								form[0].reset();
							} else {
								$('.message').html('Error: '+data). addClass('error').show();
								$('html, body').animate({ scrollTop: $($('.message')).offset().top}, 500);
							}
						});

						return false;
					});
				})();
				(function() {
					if (!$('.nav-toggle').length) {
						return;
					}
					if ($('header').hasClass('blue-header')) {
                        $('footer .logo').clone().prependTo(".main-nav");
                    } else {
                        $('header .logo').clone().prependTo(".main-nav");
                    }
					$('.nav-toggle').click(function () {
						$('.main-nav').addClass('open-nav');
						$('body').addClass('open-nav');
						return false;
					});
					$('.nav-close').click(function () {
							$('.main-nav').removeClass('open-nav');
							$('body').removeClass('open-nav');
							return false;
					});
				})();
				// Scroll To
				(function() {
					$('.scrollto').on('click', function(e){
						e.preventDefault();
						var section = $(this).attr('href');
						$('html, body').animate({
				            scrollTop: $(section).offset().top
				        }, 1000);
					});
				})();
				(function() {
					if ($('.main-contact').length) {

						var firstImg = $('.img-box img:first-child').addClass('active');
						$( ".contact-info a" ).hover(
							function() {
								$( ".contact-info li" ).removeClass('current');
								$( ".img-box img" ).removeClass('active');
								$(this).parent().addClass('current');
								var elementHover = $(this).attr("href");
								var src =  $(elementHover).addClass('active');
                                var changeBackground = $('.img-box img.'+elementHover);
                                changeBackground.addClass('active');
							}
						);
						$( ".contact-info a" ).click(function () {
							return false;
						});
					}
				})();
				(function() {
					if ($('#hero').length) {
						var heroSwiper = new Swiper('#hero .swiper-container',{
							slidesPerView: 1,
							loop: true,
							resizeFix: true,
							autoResize: true,
							resizeReInit: true,
							queueStartCallbacks: true,
							queueEndCallbacks: true,
							pagination: '.pagination',
							paginationClickable: true,
							effect: 'fade'
						});

						if(typeof(window.Clique.TypingAnimation) !== undefined){
							var typingAnimation = new window.Clique.TypingAnimation({
								container: "#animated-text",
								phrases: ["ADAPT", "INNOVATE", "EVOLVE"],
								onNextPhrase: function() {
									heroSwiper.slideNext();
								}
							});
						}
					}
				})();
				//Home page Swiper
				(function() {
					if ($('#hero1').length) {
						var heroSwiper1 = new Swiper('#hero1 .swiper-container',{
							slidesPerView: 1,
							loop: true,
							resizeFix: true,
							autoResize: true,
							resizeReInit: true,
							queueStartCallbacks: true,
							queueEndCallbacks: true,
							pagination: '.pagination',
							paginationClickable: true,
                            autoplay: 3000,
                            speed:2000,
                            onTransitionEnd: function(swiper){
                                swiper.params.speed = 2000;
                            },
                            onTouchStart: function(swiper){
                                swiper.params.speed = 400;
                            },
                            onTransitionStart: function (swiper) {
                                swiper.params.speed = 400;
                            }
						});

                        $('#hero1 .swiper-button-prev').on('click', function(e){
                            e.preventDefault();
                            heroSwiper1.params.speed = 400;
                            heroSwiper1.slidePrev();
                        });
                        $('#hero1 .swiper-button-next').on('click', function(e){
                            e.preventDefault();
                            heroSwiper1.params.speed = 400;
                            heroSwiper1.slideNext();
                        });
					}
				})();
				// All Pages Animation
				(function() {
					if ($('.animated-section').length) {
						$('.animated-section :header, .animated-section p, .animated-section img, .animated-section li, .animated-section .team-item-quote, .animated-section .btn, .animated-section .more, .animated-section .team-item-quote, .animated-section .author-name, .animated-section .date-box, .animated-section .h3, .animated-section .p, .animated-section .title-box, .animated-section .content-box, .history-section :header, .history-section img, .history-section .title, .history-section .content-slider, .team-bottom-box .tw-item-author').addClass("hidden").viewportChecker({
							classToAdd: 'visible animated fadeIn',
							offset: '20%'
						});
					}
				})();
				//globe-box
				(function() {
					if ($('.globe-box').length) {
						$('.globe-box').addClass("hidden").viewportChecker({
							classToAdd: 'visible animated fadeIn',
							offset: '50%'
						});
					}
				})();
				//triangle-box
				(function() {
					var $svg = $('.triangle-box svg').drawsvg();
					$('.triangle-box').viewportChecker({
						offset: '20%',
						callbackFunction: function(elem, action){
							$svg.drawsvg('animate');
						}
					});


				})();
				//Section-box-grid animation
				(function() {
					if ($('.section-box-grid').length) {
						$('.section-box-grid').viewportChecker({
							classToAdd: 'visible',
							offset: '30%'
						});
					}
				})();
				(function() {
					if ($('.block-zoom-item').length) {
						$('.block-zoom-item').viewportChecker({
							classToAdd: 'visible',
							offset: '20%'
						});
					}
				})();

				//Banner animation connected to fields in WP
				(function() {
					if ($('.animated-text-wrapp').length) {
					if(typeof(window.Clique.TypingAnimation) !== undefined){
                        var pagePhrasesObj = $('.data-words span').toArray();
                        var pagePhrases = new Array();
                        pagePhrasesObj.forEach(function(element) {
                            pagePhrases.push($(element).text());
                        });
						var typingAnimation = new window.Clique.TypingAnimation({
							container: ".animated-text",
							phrases: pagePhrases
						});
					}
				}
				})();

				(function() {
					if ($('.section-box-grid').length) {
						$(".section-box-grid .wrap  > .section-grid-block").css( 'transition-delay', function(i){return ((i + 1)*100)+"ms";});
					}

				})();
				//DV CULTURE gallery animation
				(function() {
					if ($('.image-gallery-section').length) {
						$('.gallery-item').viewportChecker({
							offset: '50%'
						})
					}
				})();
				// Backstretch EVERY PAGE !!!
				(function() {
					if ($('#main').length) {
                        var liveBanner = $('.top-banner').data('image');
						$.backstretch(liveBanner);
					}
				})();


				// Our History slider
				(function() {
					// -------------------------------------------------------------
					//   Basic Navigation
					// -------------------------------------------------------------
					(function() {
						var $frame = $('#basic');
						var $slidee = $frame.children('ul').eq(0);
						// $('#basic ul li:first-child').addClass('initial');
						// $('#basic ul li:nth-of-type(2)').addClass('after');
						var $wrap = $frame.parent();

						// Call Sly on frame
						$frame.sly({
							horizontal: 1,
							itemNav: 'forceCentered',
							activateMiddle: true,
							smart: true,
							activateOn: 'click',
							mouseDragging: 1,
							touchDragging: 1,
							releaseSwing: 1,
							startAt: 0,
							scrollBar: $wrap.find('.scrollbar'),
							scrollBy: 1,
							speed: 300,
							elasticBounds: 1,
							easing: 'easeOutExpo',
							dragHandle: 1,
							dynamicHandle: 1,
							clickBar: 1,
						})

						$(window).resize(function(e) {
							$frame.sly('reload');
						});
						$frame.sly('on','active',function(e,i){
                            console.log(i);
							$('#basic ul li:nth-child('+i+')').addClass('before');
							var beforeItem = i-1;
                            console.log(beforeItem);
							var afterItem = i + 1;
							$('#basic ul li:nth-child('+beforeItem+')').removeClass('before');
							$('#basic ul li:nth-child('+afterItem+').active').removeClass('before');
						});


					}());
				})();

				// Forms
				(function() {
					$('form input, form textarea').on('focus', function(){
						$(this).prev().css('opacity', '1');
					});
					$('form input, form textarea').on('blur', function(){
						$(this).prev().css('opacity', '0');
					});
				})();


				// homeHeight
				function homeHeight(){
					if ($('#hero').length) {
							var homeHeight = parseInt($('.home-main').outerHeight());
							var windowHeight = parseInt($(window).outerHeight());
							if (windowHeight > homeHeight) {
								$("#hero").height(windowHeight);
								$('.wrapper-box').css('height', windowHeight);
							} else {
								$("#hero").height(homeHeight);
								$('.wrapper-box').css('height', 'auto');
							}
					}
				}
				homeHeight();
				$(window).on('resize', function(e) {
					homeHeight();
				});

				// Reveal Menu
				(function() {
					function scrollUpDown(){
						var lastScrollTop = 0, delta = 5;
						$(window).scroll(function(){
							var nowScrollTop = $(this).scrollTop();
							if(Math.abs(lastScrollTop - nowScrollTop) >= delta){
								if (nowScrollTop < 15){
									$('.header').removeClass('fixed');
								} else {
									if (nowScrollTop > lastScrollTop && nowScrollTop > 15){
										$('.header').css({'opacity':'0', 'top':'-100px'});
										$('.header').removeClass('fixed');
									} else {
										$('.header').css({'opacity':'1', 'top':'0'});
										$('.header').addClass('fixed');
									}
								}
								lastScrollTop = nowScrollTop;
							}
						});
					}
					scrollUpDown();
				})();
			},
			finalize: function() {
				// JavaScript to be fired on all pages, after page specific JS is fired
			}
		},

		// Home page
		'page_template_template_home': {
			init: function() {
				// JavaScript to be fired on the home page
				/*var section1_waypoint_50 = new Waypoint({
					element: document.getElementById('section1'),
					handler: function(direction) {
						$('.home-about .left-angle').css({'left': '-200px'});
						$('.home-about .words-wrap').css({'left': '0'});
					},
					offset: '50%'
				});*/
				$('.info-box-item').on('hover', function(){
					$('.info-box-item').removeClass('current')
					$(this).addClass('current');
				})
			},
			finalize: function() {
				// JavaScript to be fired on the home page, after the init JS
			}
		},

		// About us page, note the change from about-us to about_us.
		'page_about': {
			init: function() {
				// JavaScript to be fired on the about us page
			}
		},

	};

	// The routing fires all common scripts, followed by the page specific scripts.
	// Add additional events for more control over timing e.g. a finalize event
	var UTIL = {
		fire: function(func, funcname, args) {
			var fire;
			var namespace = Sage;
			funcname = funcname === undefined ? 'init' : funcname;
			fire = func !== '';
			fire = fire && namespace[func];
			fire = fire && typeof namespace[func][funcname] === 'function';

			if (fire) {
				namespace[func][funcname](args);
			}
		},
		loadEvents: function() {
			// Fire common init JS
			UTIL.fire('common');

			// Fire page-specific init JS, and then finalize JS
			$.each(document.body.className.replace(/-/g, '_').split(/\s+/), function (i, classnm) {
				UTIL.fire(classnm);
				UTIL.fire(classnm, 'finalize');
			});

			// Fire common finalize JS
			UTIL.fire('common', 'finalize');
		}
	};

	// Load Events
	$(document).ready(UTIL.loadEvents);
})(window.jQuery, window.Clique); // Fully reference jQuery after this point.
