<?php
/**
 * Template Name: Careers Template
 */
?>

<?php while (have_posts()) : the_post(); ?>
    <?php get_template_part('templates/content', 'page-careers'); ?>
<?php endwhile; ?>